# http://sinatrarb.com/intro-ja.html

require 'sinatra'

eval(File.read('pv/sinatra.config')) rescue true
@configs ||= {}

require './auth' unless(@configs[:no_auth])

get('/') {
	erb :index
}

post('/') {
	erb :index
}

get('/%s/*' % (indexes = 'indexes')) {|path|
	html = <<HEADER % [it = '/%s/%s' % [indexes, path], it]
<!DOCTYPE html>
<HTML>
	<HEAD>
		<TITLE>Index of %s</TITLE>
	</HEAD>
	<BODY>
		<H1>Index of %s</H1>
HEADER

#	html << "path: [%s]<BR>\n" % path.split(/\//).inspect
#	html << "dir: [%s]<BR>\n" % ('public/%s/%s' % [indexes, path]).inspect

	pr = (path == '') ? '': indexes + '/'
	html << "\t\t<A href='/%s%s'>../</A><BR>\n" % [pr, path.split(/\//)[0..-2].join('/')]
	begin
		Dir.open('public/%s/%s' % [indexes, path]) {|dh|
			dh.sort.each {|de|
				(de == '.' or de == '..') and next
				stat = File.stat('public/%s/%s/%s' % [indexes, path, de])
				html << "\t\t<A href='/%s/%s/%s'>%s%s</A><BR>\n" % [indexes, path, de, de, stat.directory? ? '/' : '']
			}
		}
	rescue
		html << $!.to_s
	end

	html << <<FOOTER
	</BODY>
</HTML>
FOOTER

	html
}

__END__

