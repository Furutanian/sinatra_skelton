FROM fedora:39

LABEL maintainer="Furutanian <furutanian@gmail.com>"

ARG http_proxy
ARG https_proxy

# https://medium.com/nttlabs/ubuntu-21-10-and-fedora-35-do-not-work-on-docker-20-10-9-1cd439d9921
#ARG c3wahost=https://github.com/AkihiroSuda/clone3-workaround/releases/download/v1.0.0/
#ADD ${c3wahost}/clone3-workaround.x86_64 /clone3-workaround
#RUN chmod 755 /clone3-workaround
#SHELL ["/clone3-workaround", "/bin/bash", "-c"]

RUN set -x \
	&& dnf install -y \
		ruby \
		rubygem-bundler \
		ruby-devel \
		redhat-rpm-config \
		gcc \
		gcc-c++ \
		make \
		zlib-devel \
		patch \
		procps-ng \
		iputils \
		iproute \
		diffutils \
		less \
	&& rm -rf /var/cache/dnf/* \
	&& dnf clean all

# git clone sinatra_skelton しておくこと
ARG TARGET=sinatra_skelton
RUN set -x \
	&& useradd -m user \
	&& mkdir -v /home/user/${TARGET}
COPY ${TARGET}/Gemfile* /home/user/${TARGET}/
RUN set -x \
	&& chown -R user:user /home/user
USER user
WORKDIR /home/user/${TARGET}
RUN set -x \
	&& bundle config set path ../bundle \
	&& bundle install \
	&& bundle clean -V
USER root
COPY ${TARGET} /home/user/${TARGET}
RUN set -x \
	&& chown -R user:user /home/user/${TARGET} \
	&& echo 'user ALL=(ALL) NOPASSWD: ALL' > /etc/sudoers.d/user

EXPOSE 8080

# Dockerfile 中の設定スクリプトを抽出するスクリプトを出力、実行
COPY Dockerfile .
RUN echo $'\
cat Dockerfile | sed -n \'/^##__BEGIN0/,/^##__END0/p\' | sed \'s/^#//\' > startup.sh\n\
' > extract.sh && bash extract.sh

# docker-compose up の最後に実行される設定スクリプト
##__BEGIN0__startup.sh__
#
#	CONFIG=sinatra.config
#	if [ ! -e pv/$CONFIG ]; then
#		echo "# $CONFIG.sample set up."
#		cp -av $CONFIG.sample pv
#		echo "Rename 'pv/$CONFIG.sample' to 'pv/$CONFIG' and modify it."
#		echo '**** HALT ****'
#		sleep infinity
#	fi
#
#	if [ -e pv/dot.bashrc ]; then
#		cp -av pv/dot.bashrc /home/user/.bashrc
#		cp -av pv/dot.virc /home/user/.virc
#		cp -av pv/dot.gitconfig /home/user/.gitconfig
#	fi
#
#	# easy cron
#	now=`date +%s`
#	target0=$((now - now % ${CYCLE0:=604800}  + ${SCHED0:=$(( 5 * 60 +  4 * 3600 + 0 * 86400 - 378000))}))	# week
#	while [ $target0 -lt $now ]; do
#		((target0 += CYCLE0))
#	done
#
#	cd pv
#	mkdir -p '~bak/~bak2/~bak3/~bak4/~bak5'
#	cd -
#
#	s=S
#	while true; do
#		pgrep -f puma > /dev/null
#		if [ $? -ne 0 ]; then
#			echo "`date`: ${s}tart puma."
#			bundle exec rackup -P /tmp/rack.pid --host 0.0.0.0 --port 8080 &
#		fi
#		s=Res
#
#		# easy cron
#		if [ `date +%s` -ge $target0 ]; then
#			((target0 += CYCLE0))
#			echo "`date`: Job easy cron 0 started."
#			pushd pv
#			cd '~bak/~bak2/~bak3/~bak4/~bak5'
#			rm -f puma.std*; while true; do if [[ `pwd` =~ ~bak ]]; then mv ../puma.std* .; cd ..; else break; fi; done
#			pkill -HUP -F /tmp/rack.pid
#			popd
#		fi
#		sleep 5
#	done
#
##	target0=$((now - now % ${CYCLE0:=900}     + ${SCHED0:=$(( 5 * 60))}))									# 15min
##	target0=$((now - now % ${CYCLE0:=3600}    + ${SCHED0:=$(( 0 * 60))}))									# hour
##	target0=$((now - now % ${CYCLE0:=86400}   + ${SCHED0:=$(( 0 * 60 +  0 * 3600 - 32400))}))				# day
##	target0=$((now - now % ${CYCLE0:=604800}  + ${SCHED0:=$(( 0 * 60 +  0 * 3600 + 0 * 86400 - 378000))}))	# week
##	target0=$((now - now % ${CYCLE0:=2419200} + ${SCHED0:=$(( 0 * 60 +  0 * 3600 + 0 * 86400 - 378000))}))	# 4weeks
#
##__END0__startup.sh__

USER user

ENTRYPOINT ["bash", "-c"]
CMD ["bash startup.sh"]

